<?php

use Illuminate\Database\Seeder;

class UpdateCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->where('id', 1)->update([
            'name' => 'NASDAQ',
            'created_at' => date("Y-m-d H:i:s"), 
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);
        
        DB::table('categories')->where('id', 2)->update([
            'name' => 'Dow Jones',
            'created_at' => date("Y-m-d H:i:s"), 
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);
        
        DB::table('categories')->where('id', 3)->update([
            'name' => 'Gold',
            'created_at' => date("Y-m-d H:i:s"), 
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);
        
        DB::table('categories')->where('id', 4)->update([
            'name' => 'General',
            'created_at' => date("Y-m-d H:i:s"), 
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);
        

    }
}
