<?php

use Illuminate\Database\Seeder;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('companies')->insert([
            'name' => 'NASDAQ',
            'Factor' => '3',
            'Price' => '1.0',
            'created_at' => date("Y-m-d H:i:s"), 
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);
        
          DB::table('companies')->insert([
            'name' => 'Dow Jones',
            'Factor' => '4',
            'Price' => '1.0',
            'created_at' => date("Y-m-d H:i:s"), 
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);
        
         DB::table('companies')->insert([
            'name' => 'Gold',
            'Factor' => '5',
            'Price' => '1.0',
            'created_at' => date("Y-m-d H:i:s"), 
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);
        
    }
}
