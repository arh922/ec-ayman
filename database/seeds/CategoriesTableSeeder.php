<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'id' => '4',
            'name' => 'category4',
            'created_at' => date("Y-m-d H:i:s"), 
        ]);
        
      /*  DB::table('categories')->insert([
            'name' => 'category2',
            'created_at' => date("Y-m-d H:i:s"), 
        ]);
        
        DB::table('categories')->insert([
            'name' => 'category3',
            'created_at' => date("Y-m-d H:i:s"), 
        ]);
        
        DB::table('countries')->insert([
            'id' => '895',
            'capital' => 'Jerusalem',
            'citizenship' => 'Palestine', 
            'country_code' => '376', 
            'currency' => 'ILS', 
            'currency_sub_unit' => 'agora', 
            'currency_symbol' => '₪', 
            'full_name' => 'Palestine', 
            'iso_3166_2' => 'PS', 
            'iso_3166_3' => 'PS', 
            'name' => 'Palestine', 
            'region_code' => '142', 
            'sub_region_code' => '145', 
            'eea' => '0', 
            'calling_code' => '970', 
            'flag' => 'PS.png', 
        ]);
        
         */                    
                              
    }
}
