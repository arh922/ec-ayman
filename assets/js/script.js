$( document ).ready(function() {  

    function nl2br (str, is_xhtml) {   
        var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';    
        return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
    }

    $( "#addCommentForm" ).submit(function( event ) {
            event.preventDefault();
            var formData = new FormData(this);
            
            $('#body_error').html(''); 
            $('#error_error').html(''); 
            
            $.ajax({
               url: $(this).attr("action"),
               type: $(this).attr("method"),
               data: formData,
               processData: false,
               contentType: false,
               success: (function(res){   
                 $('textarea[name="body"]').val('');        
                 $('.no-comment').attr('style', 'display:none');  
                                
                 var newComment = '<div class="marginBottom15 comment"> ' + nl2br(res.body, false) + '<div class="post-meta" style="margin-top: 10px">';
                 newComment += '<span><i class="fa fa-clock-o"></i> ' + res.created_at +' </span><span><i class="fa fa-user"></i> ' + res.name;
                 newComment += '</span></div></div>';
                 
                 $('#commentCount').html( parseInt($('#commentCount').html())+1);
                 
                 $('#priceDiv').html(res.price);
                 
                 $('.comment:last').after(newComment);           
            }) ,
            error: (function(err) {     
                  $( "[id*='_error']" ).removeClass('required');
                  $( "[id*='_error']" ).attr('style', 'display:none');
                  $.each(err.responseJSON, function( index, value ) {
                        var errorDiv = '#' + index + '_error';
                        var errorInput = '#' + index + '_input';
                        $(errorDiv).addClass('required');
                        $(errorInput).addClass('required-input');
                        
                         $( errorDiv ).attr('style', 'display:block'); 
                        
                        $(errorDiv).empty().append(value);
                  });  
              })        
            }) 
            
      });
});