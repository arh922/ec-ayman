<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Auth::routes();

Route::get('/index', 'HomeController@index');
Route::get('/home', 'HomeController@index');
Route::get('/', 'HomeController@index');

Route::group(['middleware' => ['auth'] ], function () { 
    Route::post('comment/{id}/add', 'CommentsController@store'); 
    Route::get('article/add', 'ArticlesController@create'); 
    Route::post('article/add', 'ArticlesController@store'); 
});

Route::get('/article/{id}/{name?}', 'ArticlesController@show');
Route::get('/category/{id}/{name?}', 'CategoriesController@show'); 