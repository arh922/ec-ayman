@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <center> <h1>NOT FOUND - 4O4</h1></center>
            <center> <a href="{{ url('/') }}">Home</a></center>
        </div>   
    </div>
</div>
@endsection
