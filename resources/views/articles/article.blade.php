@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading"><h1>{{$article->title}}</h1></div>

                <div class="panel-body">
                    {{ $article->body }}
                    
                    @include('partials.articleInfo')  
                    
<!--                    <div class="form-group">-->
                        <div class="marginTop30"><h2>Comments:</h2></div>
                        <div class="col-md-12 marginTop30">
                            @include('partials.comments')
                        </div>
<!--                    </div>-->
                    
                    <div class="col-md-12 marginTop30">
                        @include('partials.addComment')
                    </div>
                 
                </div>
                
                
            </div>
        </div>
        
        @include('partials.categoriesSideBar') 
        
        
    </div>
</div>
@endsection
