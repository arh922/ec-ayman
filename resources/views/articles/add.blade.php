@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <div class="panel panel-default">
                <form id="addProductForm" class="form-horizontal" role="form" method="POST" action="{{ url('article/add') }}">
                <fieldset>

                    <legend><div class="panel-heading">Add Article</div> </legend>
                    
                    {!! csrf_field() !!}
                   
                    @include('partials.article')
                                
                    <div class="separator"></div>
                    
                    <div class="form-group">
                      <label class="col-md-2 control-label" for="singlebutton"></label>
                         <div class="col-md-6">
                            <button id="addProductBtn" type="submit" class="btn btn-primary">
                                Add Article
                            </button>
                            
                            <a class="btn btn-primary"href="{{ url('/') }}">
                               Cancel
                            </a>
                        </div>  
                    </div>
                
                </fieldset>
            </form>
            </div>
        </div>
        
        @include('partials.categoriesSideBar')
              
    </div>
</div>
@endsection
