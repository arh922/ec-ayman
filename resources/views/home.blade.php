@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">Articles ...</div>

                <div class="panel-body">
                    @if($articles->count())
                        @foreach($articles as $article)
                             @include('partials.articles')
                        @endforeach
                    @else
                       <div>No Articles ...</div>
                    @endif
                    
                    <div class="col-md-12"><?php echo $articles->render(); ?> </div>
                     
                </div>
            </div>
        </div>
        
        @include('partials.categoriesSideBar')
              
    </div>
</div>
@endsection
