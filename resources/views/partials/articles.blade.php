<div class="items-list">
    <div class="blog-wrapper">
       
        <div class="blog-title">
            <a class="category_title" href="{{ url('/category/' . $article->category->id . '/'. $article->category->name) }}" title="">{{ $article->category->name }}</a>
            <h2>
               <a href='{{ url("/article/" . $article->id . "/" . str_replace(" ", "-", $article->title))}}' title="">
                  {{ $article->title }} 
               </a>
            </h2>
                    
            <p>{!! mb_substr($article->body, 0, 300, 'UTF-8') !!} </p>
            
            @include('partials.articleInfo') 

        </div><!-- end desc -->
    </div><!-- end blog-wrapper --> 
    
    <hr />
</div>