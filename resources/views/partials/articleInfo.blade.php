<div class="post-meta">
    <span>
         <i class="fa fa-clock-o"></i>
        {{$article->created_at}}
    </span>
    
    <span>
         <i class="fa fa-comments"></i>
        <lable id="commentCount">{{$article->comments->count()}}</lable> Comments
    </span> 
    
    <span>
         <i class="fa fa-tag"></i>
         {{$article->category->name}}
    </span> 
    
    <span>
         <i class="fa fa-money"></i>
         <lable id="priceDiv">
             @if (isset(App\Http\Models\Company::where('name', $article->category->name)->first()->price))
                 {{round(App\Http\Models\Company::where('name', $article->category->name)->first()->price, 3)}}
             @else
                 [No price]
             @endif
         </lable>
    </span> 
</div>