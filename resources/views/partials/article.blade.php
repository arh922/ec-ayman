<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
    <label class="col-md-2 control-label" for="textinput">Title</label>  
    <div class="col-md-9">
        <input value="{{ @$article->title }}" id="title" name="title" placeholder="Title" class="form-control input-md" type="text">
         @if ($errors->has('title'))
            <span class="help-block">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
         @endif
    </div>
</div>


<div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}"> 
      <label class="col-md-2 control-label" for="textinput">Categories</label>                
      <div class="col-md-9">
         {!! Form::select('category_id', App\Http\Models\Category::pluck('name', 'id'), @$article_categories,['id' => 'categoryId', 'class'=> 'form-control']) !!}       
         @if ($errors->has('category_id'))
            <span class="help-block">
                <strong>{{ $errors->first('category_id') }}</strong>
            </span>
         @endif
    </div>
</div> 

<!-- Textarea -->
<div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
    <label class="col-md-2 control-label" for="textarea">Body</label>
    <div class="col-md-9">                     
       <textarea class="form-control" id="body" name="body" placeholder="Body">{{@$article->body}}</textarea>
      @if ($errors->has('body'))
            <span class="help-block">
                <strong>{{ $errors->first('body') }}</strong>
            </span>
         @endif
    </div>
</div>
