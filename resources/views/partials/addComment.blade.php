@if (Auth::check())
    <div class="widget-title">
        <h4>Write your a comment here ....</h4>
    </div>
    <div class="commentform">
        <form id="addCommentForm" class="form-horizontal" role="form" method="POST" action="{{ url('comment/' . $article->id . '/add') }}"> 
             {!! csrf_field() !!} 
            <textarea name="body" class="form-control" placeholder="Write a comment ..." id="addTextArea"></textarea>
            <div id="body_error"></div>
            <div id="error_error"></div> <br />
           
            <div class="form-group">
                <div class="col-md-2"><input id="addCommentBtn" type="submit" value="Send" class="btn btn-primary btn-block" /></div>
            </div>
            
        </form>
    </div>
@endif