<div class="col-md-3">
   <div class="panel panel-default">
        <div class="panel-heading">Categories ...</div>
        <ul>
        @foreach($all_categories as $cat) 
           <li> <a href="{{url('category/' . $cat->id . '/' . $cat->name)}}">{{$cat->name}}</a> ({{$cat->articles->count()}})</li>
        @endforeach  
        </ul>
    </div>
</div>