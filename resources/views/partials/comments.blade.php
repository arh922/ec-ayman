@if ($comments->count())
    @foreach ($comments as $comment)
        <div class="marginBottom15 comment">
             {!! strip_tags( nl2br($comment->body), '<br>') !!} 
             
             <div class="post-meta" style="margin-top: 10px">
                <span>
                     <i class="fa fa-clock-o"></i>
                    {{$comment->created_at}}
                </span>
                
                <span>
                     <i class="fa fa-user"></i>
                     {{$comment->user->name}}
                </span> 
            </div>
        </div>  
    @endforeach
@else
    <div class="marginBottom15 comment no-comment">No comments so far.</div>
@endif