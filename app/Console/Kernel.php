<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

use App\Http\Models\Company; 

use Log;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        
         $schedule->call(function () {
              $companies = Company::all();
              $this->changePriceEveryFiveSec($companies);
                
        })->everyMinute();                     
    }
    
    private function changePriceEveryFiveSec($companies){
          $this->changePrice($companies, 5);
          sleep(5);
          $this->changePrice($companies, 10);
          sleep(5);
          $this->changePrice($companies, 15);
          sleep(5);
          $this->changePrice($companies, 20);
          sleep(5);
          $this->changePrice($companies, 25);
          sleep(5);
          $this->changePrice($companies, 30);
          sleep(5);
          $this->changePrice($companies, 35);
          sleep(5);
          $this->changePrice($companies, 40);
          sleep(5);
          $this->changePrice($companies, 45);
          sleep(5);
          $this->changePrice($companies, 50);
          sleep(5);
          $this->changePrice($companies, 55);
          sleep(5);
          $this->changePrice($companies, 60); 
    }
    
    private function changePrice($companies, $c){  
        echo $c . ' secs' . "\n";
        foreach($companies as $company) {
              $price = $company->price;
              $factor = $company->factor;
              
              $new_price = ($price/$factor)*2.5;
              
              if (round($new_price, 2) == 0.01) $new_price = '1.0';
              
              $company->price = $new_price;
              $company->save();
        
              echo $company->name . '->' . $company->price . "\n";    
          }
          
           echo '--------------------------------------------------------------------------' . "\n"; 
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
