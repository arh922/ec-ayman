<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check(); 
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (is_numeric($this->segment(3))) {
            $validation = 'required|unique:categories,name,' . $this->segment(3);
        }
        else{
            $validation = 'required|unique:categories';    
        }
            
        return [
            'name' => $validation
        ];
    }
}
