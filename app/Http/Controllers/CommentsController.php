<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\CommentRequest;

use App\Http\Models\Article;
use App\Http\Models\User;
use App\Http\Models\Comment;
use App\Http\Models\Company;

use Auth;

class CommentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommentRequest $request, $id)
    {
        $user_id = Auth::user()->id;

        $body = $request->input('body');
        
        $article = Article::findorFail($id);
                
        $comment = new Comment(['user_id' => $user_id, 'body' => $body]);
        $new_id = $article->comments()->save($comment);
              
        $new_comment = Comment::findOrFail($new_id->id);
        $new_comment['name'] = Auth::user()->name;      
        $new_comment['price'] = isset(Company::where('name', $article->category->name)->first()->price) 
                                ? 
                                round(Company::where('name', $article->category->name)->first()->price, 3):'[No price]';      
        
        return $new_comment; 
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
