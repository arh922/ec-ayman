<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

use Carbon;

class Article extends Model
{
    protected $fillable = ['title', 'body', 'category_id']; 
    
    public function category(){
        return $this->belongsTo('App\Http\Models\Category');
    }
    
    public function comments(){
        return $this->hasMany('App\Http\Models\Comment');
    }
    
     public function getCreatedAtAttribute($date){
        return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y');
    }  
}
