<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

use Carbon;

class Comment extends Model
{
    protected $fillable = [
        'body', 'article_id', 'user_id'
    ];
    
    public function article(){
        return $this->belongsTo('App\Http\Models\Article');
    }
    
     public function user(){
        return $this->belongsTo('App\Http\Models\User');
    }
    
     public function getCreatedAtAttribute($date){
        return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y');
    } 
}
